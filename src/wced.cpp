// wced.cpp

// Copyright 2022 Michele Schimd

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <str/distance.hpp>
#include <alignment.hpp>

using namespace wced;

int
main(int argn, char** argv) {
	auto ed = ctl::make_band_linear_alg(10,10,5);
	std::string x("AA");
	std::string y("AAF");
	std::cout << ed(x.begin(), x.end(), y.begin(), y.end()) << "\n";
	std::vector<size_t> v1 = {1,2};
	std::vector<size_t> v2 = {1,3};
	Alignment<> al(v1.begin(), v1.end(), v2.begin());
	std::cout << al.size() << "\n";
	for (auto it = al.cbegin(); it != al.cend(); it++) {
		std::cout << (*it).first << "\t" << (*it).second << "\n";
	}
	return 0;
}