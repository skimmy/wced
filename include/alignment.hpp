// alignment.hpp

// Copyright 2022 Michele Schimd

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef WCED_ALIGNMENT_HPP
#define WCED_ALIGNMENT_HPP

#include <vector>

namespace wced {

// According to [LMT13] e [BS22], an alignment is a pair of integers sequence
template <class IndexT = size_t>
class Alignment {
	using AlignedPosition = std::pair<IndexT, IndexT>;
	using AlignmentT = std::vector<AlignedPosition>;
	using const_iterator = typename std::vector<AlignedPosition>::const_iterator;
public:
	template<class IterT>
	Alignment(IterT b1, IterT e1, IterT b2) {
		while(b1 != e1) {
			alignment.push_back(std::make_pair(*b1, *b2));
			b1++;
			b2++;
		}
	}

	size_t
	size() const {
		return alignment.size();
	}

	const_iterator
	cbegin() {
		return alignment.cbegin();
	}

	const_iterator
	cend() {
		return alignment.cend();
	}
private:
	std::vector<AlignedPosition>alignment;
};

}

#endif